# Requirements
MUST is core, minimal viable product
SHOULD is desired features
WON'T is banned features
| is reason for decision

# general
 - user must open bot with link https://t.me/squid_squad_bot
 - user must read interface in russian
 - user should read interface in uzbek
 - user should read interface in english
 - user must feel like owner of project
 - user must collaborate with other users
 - user must learn skills every mission
 - user should read lore, story, game characters
 - user won't feel like ceo of large cex
 - user won't feel like they are examinated
 - user won't watch ads
 - user must see source code with open source license
 - user must see public documentation
 - user must see project telegram channel
 - user must see project landing website
 - user must see qr code when open game link in browser
 - user must see qr code when open game link in desktop telegram
 - user must receive airdrops for high hourly rate
 - user must understand how missions improve skills
 - user must understand how skills increase hourly score
 - user must be able to get higher score than players who started to play earlier
 - user won't cheat with taps
 - user won't cheat with missions
 - user must feel that other user rating is earned with intelligence
 - user must experience friendly interactions with other users 
 - user must feel that other users are real authenticated humans
 - user must feel the same level of engagement as hamster kombat
 - user must feel like they have a lot of money and can use it in many ways
 - user must see game score in game currency
 - user won't see game score in dollars
 - user should see daily quiz in every telegram channel
 - user should share ansewer to daily quiz with others
 - user must see dark background screen
## technical
 - server must withstand 15k day increase in users
# main screen
## ux
 - user must click back button to close bot
 - user must read bot name "squid squad" in the header
 - user must read account name at the top left
 - user must see shortened label if account name is too long
 - user must see level number at the top left
 - user must see level name under account name at the top left
 - user must see the score at the top center in bold font
 - user must see a coin with an octopus icon left of the score
 - user must see the score at the center when score becomes longer, the icon further left
 - user won't see the score at the topmost of the screen, out of attention
 - user must see a large tap button with cute three dimensional octopus icon center of the screen
 - user won't see account avatar profile picture | hard to implement
 - user must see shadow on the tap button
 - user must tap the tap button
 - user must see a "+1" animation over the tap button after each tap
 - user won't see "+1" animation when energy is at 0 
 - user must see the number of energy points left at the bottom of the screen
 - user must see the maximum energy points at the bottom of the screen
 - user must see a lightning icon left of the number of energy points
 - user must feel vibration after each tap
 - user won't hear sound notifications
 - user must see the highlighted tab "tap" at the bottom of the screen
 - user must see the tab "friends" at the bottom of the screen
 - user should see how many points left to increase level
## technical
 - user must see increased score and decreased energy after tap
 - user won't see changed score when energy is at zero
 - user must see the energy replenish by a point every 3 seconds
 - user must restore complete energy in 3 hours
 - user must sync collected taps to the server when tap stops
 - user must cache taps in local storage
 - user must save taps to cache if they close bot immediately after tap
 - user must sync taps from cache on startup
 - user must see large score number, millions, to get dopamine
 - user must know that tapping is not an endless job, that energy is finite
 - user won't choose the centralized exchange affiliation
## ideas
 - user should confirm notification to get daily award
 
# invite screen
## ux
 - user must click back button to close bot
 - user must read bot name "squid squad" in the header
 - user must read call to action to invite friends at the top
 - user must read promise to get bonus score after invite
 - user must see list of friends that accepted invitation
 - user must see name of invited friend
 - user must see level of invited friend
 - user must see amount of bonus score points received for friend ????
 - user must click button "invite" to open telegram message dialog with a referral link
 - user must click button "copy" to copy referral link to copypaste buffer
 - user must see tab "tap" at the bottom of the screen
 - user must see the highlighted tab "friends" at the bottom of the screen
 - user won't see list of other friends who installed bots | because hard to implement
## technical
 - user must copy a referral link https://t.me/squid_squad_bot/start?startapp=kentId162832046
 - user must see user that invited them in the friends list
 - user must help novices gain skills
 - user must get score points for helping novices
 - user must see invited people as a clan, underlings
 
# mission screen
 - user must click back button to close bot
 - user must read bot name "squid squad" in the header
 - user must read header with mission name
 - user must confirm notification to collect points
 - user must return to main screen after mission is complete
 - user must see stories with instructions for complex missions
 - user must pass complete simple mission without stories
 - user must see timer loader over each story
 - user must click to next story only after timer is complete
 - user must click back button even before timer is complete
 - user won't complete task before they read every story
 - user should repeat each mission
 - user should get more points for first pass of mission 
 - user should get less points for subsequent pass of mission 
 - user should get hourly rate increased score for each mission
 - user should spend score to upgrade skill and increase hourly rate
 - user should pass quiz to upgrade skill and increase hourly rate 
 - user should get boost for taps after passing a mission
## missions
 - open wallet
   - user must follow link to install wallet
   - user must connect wallet
 - p2p
 - bybit
 - binance
 - buy token
 - exchange token
 - twitter
 - staking ton space
 - decentralized trading
 - centralized trading
 - emit fungible token
 - emit non fungible token
 - subscribe to telegram channel
 - subscribe to twitter
 - kyc
 - da
 - defi
 - socialfi
## technical
 - user must be verified with twitter app social authentication 
 - user must see one new mission
 - user can see story with screencast video/gif autoplay with instruction, no sound, with cursor
 - user can see story with iframe
 - user can see story with gpt chat and a prompt
 - user can see story with quiz
 - user can see story with animation
 - user can see story with text
 - user can see story with layout
 - user can see story with voice
 - user must get return on investment for each upgrade from hourly rate 
   - upgrade for 5k, get 1k per hour, return in 5 hours

# rating screen
 - user must see a list of users with higher rating
 - user must see a list of users with lower rating
 - user must see a list of users with top highest rating
 - user must click on user to see their skills and missions
 - user can get temporary boost to high rating after passing a mission
 - user can tap in squad to retain high rating after passing a mission
 - baby, intern, beginner, bronze, silver, legendary, master, yellow, red, orange, green, teal
 - peer group, class, your grade
## technical
 - user must define rating by hourly rate

# squad screen
 - user should chat to squad members
 - user should join arcoiris mission 
 - user should join squad
 - user won't promote their channel with squad
 - user must bet score points to participate in squad
 - user must participate in p2p game, click for a minute, if both go to 100%, both get points
 - user must get more points when in squad with higher level user
 - user must win points if both users completed the squad
 - user must lose points if one of users left the squad before completion
 - user won't pvp battle
 - user should help and get points
 - squad, syndicate, mafia, 
 
# token screen
 - user should buy tokens with score
 - user should see tokens for score
 - user should exchange tokens on simulated automated market maker
 - user not connect to ton tokens, but simulate blockchain instead
 - user can mine ton coins with hourly rate
 - user can mine simulated coins with hourly rate
 - user can receive nft for game achievements
 - user can see charts from coinmarketcap api
 - user won't pay to win with tokens
## technical
 - every TON NFT is a separate contract
 - websocket for price datafeeds

# user portraits
 - user must feel banked
 Lena is unbanked, has no bank account wants to feel financial freedom
 Gena doesn't want to compete
 Pete wants to compete
