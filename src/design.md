# Design
a clicker that teaches you 60% of web3 knowledge

incremental game, score increases as you tap
idle game, score increases in the background
multiplayer game, score increases from collaboration with users
web3 game, players convert score into tokens and trade on a virtual exchange

at first you're cute little baby squid, learn and work with others to become the kraken

## interacts
telegram user

telegram API

playdeck

ton lightserver

## constitutes
clicker, incremental game Telegram Mini App with web3 incentives

## includes 
telegram miniapp

ton token

## competes
hamster kombat

doxcoin

notcoin
## resembles
[buildyourstax](https://buildyourstax.com)

virtonomics

gton course

[universal paperclips](https://www.decisionproblem.com/paperclips/index2.html)

https://universalpaperclips.fandom.com/wiki/Universal_Paperclips_Wiki

https://github.com/cryptomi-io/bot.cryptomi.hub

https://github.com/KatasonovYP/rc-tg-ton-timesheets

https://t.me/pixelversexyzbot
https://github.com/bitcraft3r/0xVenture-Capitalist
https://github.com/simlees/capital
https://github.com/spumas/CapitalismSimulator
https://github.com/NathanJargon/goodbyeCapitalism
https://github.com/TrogdorIII/CapitalismClicker
https://github.com/BlackPhoenix134/CartelClicker

## target audience
parents

## mission
educate 50 mln people

## stakeholder
noblescript, toncoin.fund, Norcivilian Labs

# stack
## interface
vuejs | team resources, react's popularity is insignificant, TelegramUI react-first is not needed

typescript | team resources and peer pressure, javascript can add integration overhead with ts-first libraries

vite | state-of-the-art, webpack is slower

ton | recommended. tonweb is older, tonkite and tonutils are too low-level

vanilla design | Mark42 has few components, TelegramUI is react-only, TeleVue only has few components. but if we write isomorphic components, should contribute them to televue

vue-tg | team resources, vanilla is script-first, @twa-dev/sdk is react-first

## storage
laravel 

mysql

server side rendering

first render on server instance $450/month, direch http response

server is idle, no activity at night gmt+4

10-15 users in queue

low priority features on serverless aws
# security
how can be cheated
 - raw editing of points | storage validated, inacessible to user
 - double missions | storage validated, inacessible to user
 - false referrals | validated by telegram api
 - mock network subscriptions | expensive to verify

# tokenomics

places where user earns score
 - tap
 - hourly rate 
 - invite bonus
 - squad game

places where user spends score
 - buy upgrade
 - risk in squad
 - buy token
 - squad game

hamster
 - tap to buy token
 - spend token to buy rate card
 - get token from hourly rate
 - spend token to tap more
