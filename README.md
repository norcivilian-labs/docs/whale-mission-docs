# squid-squad-docs

Documentation for [squid squad](https://gitlab.com/norcivilian-labs/squid-squad), built with [mdBook](https://github.com/rust-lang/mdBook) and hosted at [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
